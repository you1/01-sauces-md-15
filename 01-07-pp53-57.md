---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}-->



## 合わせバター  {#beurres-composes}



\begin{center}
\textbf{グリル、ソースの補助材料、オードブル用}
\end{center}
\vspace{1zw}

<!--<div class="frsecenv">Beurres Composés pour Adjuvants de Sauces et Hors-d'oeuvre</div>-->

\index{あわせはた@合わせバター|(}
\index{はた@バター|see{合わせバター}}
\index{ふるこんほせ@ブール・コンポゼ|see{合わせバター}}
\index{みつくすはた@ミックスバター|see{合わせバター}}
\index{beurre@beurre!beurres composes@---s Composés|(}

### \hspace*{-1zw}概説 {#observation-sur-les-beurres-composes}



本書においてレシピを掲載している合わせバター[^1]のうちのほとんどは、甲
殻類の合わせバターを除いて、料理に直接用いられることがとても少ない。だ
が、合わせバターはさまざまなシチュエーションで役に立つ。ポタージュでは
野菜の合わせバターが、その他の合わせバターはソース作りにおいて有用だ。
ソースの風味と性格を明確に伝える決め手になるからだ。



だから、読者である料理人諸君には、ここに書いてあることを真剣に読みとっ
ていただきたい。


甲殻類のバターについては、経験上、\ruby{湯煎}{バンマリ}にかけながら煮出して[^2]から、氷
水で冷やした陶製の容器に布で\ruby{漉}{こ}し
入れるといい。そうすれば、冷たい状態で
作るよりも赤みがきれいに出る。だが逆に、熱によって風味の繊細さが失なわ
れてしまい、雑味さえも出てしまう。

この問題点を解決するために、我々は二種類の違うバターを作るという方式を
採ることにした。ひとつは甲殻類の胴のクリーム状の部分と切りくずあるいは
身そのものを生のバターとともに鉢ですり潰して、目の細かい網で裏ごしする
か、布で\ruby{漉}{こ}すというもの。このバターはソースに完璧ともいうべき風味を添え
てくれる。とりわけベシャメルソースをベースとしたソースの場合はそうだ。


もうひとつは、甲殻類の殻だけを用いて、熱して作るものだ。これは「色付け」
の役割しか持たない。この方式はまことに素晴しい結果を得られるので、ぜひ
とも実行していただきたい。

場合によっては、我々はバターを同様の上等な生クリームに代えることがある。
生クリームのほうがバターよりも、素材の持つ風味や香気をよく吸収する。こ
うすればソースやポタージュの仕上げに加えるのに文句ないクリ[^3]を作ることが
出来るわけだ。

色付け用のバターを使うと、ソースがきれいに色付き、個性的なソースとなる。
どんな場合でも、カルミン色素[^4]よりもずっといい。カルミン色素はソース
やポタージュにくすんだ、なさけない色合いしか与えてはくれないのだ。

合わせバターは一般的に、使う際にその都度作る[^5]ものだが、作り置き
しておかなければならない場合は、白い紙で円筒形に包んで冷蔵保管すること。





[^1]: beurre composé [ブール](#beurre-gls)・コンポゼ。ミックスバ
    ターとも。合わせバター（具体的にはアンチョビバター、エクルヴィスバ
    ターなど）への言及は1806年刊ヴィアール『帝国料理の本』に既に見られ
    る（この初版、第二版では残念なことにレシピそのものは記載されていな
    い)。つまり合わせバターという概念および仕込んでおくものという認識
    は19世紀初頭までに成立していたといえる。


[^2]: infuser （[アンフュゼ\*](#infuser-gls)）。

[^3]: coulis （クリ）水分のやや多いピュレをイメージするといい。「クー
    リ」と呼ぶ日本の調理現場は多い。

[^4]: コチニール色素ともいう。ラックカイガラムシなどを原料として抽出し
    た色素。ヨーロッパでは古代から中世にかけてケルメスカイガラムシから
    抽出され利用されてきた、非常に歴史の古い色素。とりわけルネサン期に
    は高級毛織物の染料として需要が高まった。また絵の具にも使用された。
    その後、ウチワサボテンでエンジムシを大量に養殖していた中南米を支配
    下に置いたスペインが、これを新大陸産のカルミンとしてヨーロッパ各国
    に売ることで巨万の富を得たという。かつて食品工業において多用された。
    1838年の『ラルース・ガストロノミック』初版では、「コチニールから抽
    出される鮮かな赤色色素で毒性はない。多くの食品に着色料として用いら
    れている」とある。現在は使用が減りつつあり、代替品としてビーツから
    抽出したビートレッドなどが増えてきている。また、この本文でカルミン
    色素の使用を「くすんだ、情けない色合いを与える」として否定的に扱っ
    ているのは、この色素がpHによって色調が変化し、なおかつ蛋白質を多く
    含む料理に加えると色素自体が紫色に変化する（結果としてソースやポター
    ジュ全体が濁ったような色になる）ことがあるためだろう。


[^5]: 原文 au moment （オモモン）その都度、の意。à la minute （アラミニュッ
    ト）と呼ぶ調理現場もある。




<div class="recette">


#### にんにくバター {#beurre-d-ail}

<div class="frsubenv">Beurre d'Ail</div>



\index{あわせはた@合わせバター!にんにくはた@にんにくバター}
\index{にんにく@にんにく!はた@---バター}
\index{beurre@beurre!--- d'ail@--- d'Ail}
\index{ail@ail!beurre@Beurre d'---}



皮をむいたにんにく200 gを強火でしっかり[ブロンシール\*](#blanchir-gls)する[^6]。よく湯をきってから、鉢
に入れてすり潰し、バター250 gと合わせ、布で\ruby{漉}{こ}す。

[^6]: 生のにんにくには胃腸を刺激する酵素が含まれているが、熱により不活
    性化するので、よく火を通す必要がある。





\atoaki{}

#### アンチョビバター {#beurre-d-anchois}

<div class="frsubenv">Beurre d'Anchois</div>

\index{はた@バター!あわせはた@合わせバター!あんちよひはた@アンチョビバター}
\index{あわせはた@合わせバター!あんちよひはた@アンチョビバター}
\index{あんちよひ@アンチョビ!はた@---バター}
\index{beurre@beurre!anchois@--- d'Anchois}
\index{anchois@anchois!beurre@Beurre d'---}




アンチョビのフィレ200 gをよく洗い、しっかり水気を絞る。これを鉢に入れ
て細かくすり潰す。バター250 gを加えて布で\ruby{漉}{こ}す。








\atoaki{}

#### アーモンドバター {#beurre-d-amande}

<div class="frsubenv">Beurre d'Amande</div>



\index{あわせはた@合わせバター!あもんとはた@アーモンドバター}
\index{あもんと@アーモンド!はた@---バター}
\index{beurre@beurre!amande@--- d'Amande}
\index{amande@amande!beurre@Beurre d'---}




アーモンド[^7]150 gを湯むきしてよく洗い、すぐに水数滴を加えてすり潰し
てペースト状にする。これをバター250 gと混ぜ合わせ、布で\ruby{漉}{こ}す。

[^7]: アーモンドには一般的なスイートアーモンド amandes doucesと、苦味
    のあるビターアーモンドamande amèresの二種がある。後者はごく微量の
    青酸化合物を含むのであまり多く使われることはないが、香りがいいため
    リキュールなどの香り付けにごく少量が用いられることがある。











\atoaki{}

#### ブール・ダヴリーヌ[^8] {#beurre-d-aveline}


<div class="frsubenv">Beurre d'Aveline</div>


\index{あわせはた@合わせバター!ふるたうりぬ@ブール・ダヴリーヌ}
\index{あうりぬ@アヴリーヌ!ふる@ブール・---}
\index{へせるなつつ@ヘーゼルナッツ|see {アヴリーヌ}}
\index{beurre@beurre!aveline@--- d'Aveline}
\index{aveline@aveline!beurre@Beurre d'---}






アヴリーヌ150 gを焙煎して丁寧に皮をむく。油が浮いてこないよう水を数滴
加えてペースト状にすり潰す。これとバター250 gを混ぜ合わせる。目の細か
い網で裏ごしするか、布で\ruby{漉}{こ}す。


[^8]: Aveline（アヴリーヌ）はヘーゼルナッツの仲間でセイヨウハシバミの大粒な変種。
    イタリア、ピエモンテ産やシチリア産が有名。







\atoaki{}

#### ブール・ベルスィ[^9] {#beurre-bercy}

<div class="frsubenv">Beurre Bercy</div>


\index{あわせはた@合わせバター!ふるへるすい@ブール・ベルスィ}
\index{へるすい@ベルスィ!ふる@ブール・---}
\index{beurre@beurre!bercy@--- Bercy}
\index{bercy@Bercy!beurre@Beurre ---}




白ワイン2 dLに細かく[アシェ\*](#hacher-gls)したエシャロット大さじ1杯を
加えて半量になるまで煮詰める。生\ruby{温}{ぬる}い程度まで冷ましてから、ポマード状に
柔らかくした[^49]バター 200 gを混ぜ込む。牛骨髄 500 gを[デ
\*](#des-gls)に切って[ポシェ\*](#pocher-gls)する。よく湯ぎりをして加え
る。パセリのアシェ大さじ1杯と塩 8 g、挽きたてのこしょう 1 つまみ強とレモ
ン $\frac{1}{2}$ 個分の果汁を加えて仕上げる。

[^9]: [ソース・ベルシー](#sauce-bercy)訳注参照。




[^49]: ポマードは昔よく用いられた整髪料だが、現代では珍しいものとなっ
    てしまった。ただ、この表現は定型句のひとつであるとともに、現代日本
    の調理現場で現在もこの表現を用いるところがあるため、あえてこの訳
    語を採用した。意味としては「指がすっと入る程度の柔らかさ」であり、
    シリコンゴムのヘラなどで混ぜやすいけれども、溶けて液体にはなってい
    るわけではない状態のことを意味している。




\atoaki{}

#### キャビアバター {#beurre-de-caviar}

<div class="frsubenv">Beurre de Caviar</div>


\index{あわせはた@合わせバター!きやひあはた@キャビアバター}
\index{きやひあ@キャビア!はた@---バター}
\index{beurre@beurre!caviar@--- de Caviar}
\index{caviar@caviar!beurre@Beurre de ---}





圧縮キャビア[^11]75 gを細かくすり潰す。パター250 gを加えて、布で\ruby{漉}{こ}す。

[^11]: もとはロシアで雪の中の樽で保存するために圧縮したもの。キャビア
    のグレードはベルガ、オセトラ、セヴルガが混ざっているのが多いという。
    比較的安価に利用できる。







\atoaki{}

#### ブール・シヴリ、ブール・ラヴィゴット[^12] {#beurre-chivry}

<div class="frsubenv">Beurre Chivry, ou Beurre Ravigote</div>


\index{あわせはた@合わせバター!しうり@ブール・シヴリ}
\index{あわせはた@合わせバター!らういこつと@ブール・ラヴィゴット}
\index{しうり@シヴリ!ふる@ブール・---}
\index{らういこつと@ラヴィゴット!ふる@ブール・---}
\index{beurre@beurre!chivry@--- Chivrya}
\index{beurre@beurre!ravigote@--- Ravigote}
\index{chivry@Chivry!beurre@Beurre ---}
\index{ravigote@ravitote!beurre@Beurre ---}






パセリの葉とセルフイユ、エストラゴン、シヴレット、若摘みのサラダバーネッ
ト100 gを数分間[ブロンシール\*](#blanchir-gls)し、水にさらしてから圧して余分な水気を絞る。エシャ
ロットの[アシェ\*](#hacher-gls)25 gもブロンシールする。これらを鉢に入れてすり潰す。

バター125 gを加え、布で\ruby{漉}{こ}す。



[^12]: それぞれの名称などについては、[ソース・シヴリ](#sacue-chivry)お
    よび[ソース・ラヴィゴット](#sauce-ravigote)訳注参照。




\atoaki{}

#### ブール・コルベール[^14] {#beurre-colbert}

<div class="frsubenv">Beurre Colbert</div>



\index{あわせはた@合わせバター!ふるこるへる@ブール・コルベール}
\index{こるへる@コルベール!ふる@ブール・---}
\index{beurre@beurre!colbert@--- Colbert}
\index{colbert@Colbert!beurre@Beurre ---}







[メートルドテルバター](#beurre-a-la-maitre-d-hotel)200 gに、溶かした[グラス
ドヴィアンド](#glace-de-viande)大さじ2杯と細かく[アシェ\*](#hacher-gls)したエストラゴン小さ
じ2杯を加える。

[^14]: [ソース・コルベール](#sauce-colbert)本文および訳注参照。







\atoaki{}

#### 色付け用の赤いバター {#beurre-colorant-rouge}

<div class="frsubenv">Beurre Colorant rouge</div>

\index{あか@赤!いろつけようのはた@色付け用の--いバター}
\index{あわせはた@合わせバター!いろつけようのあかいはた@色付け用の赤いバター}
\index{ちやくしよくそざい@着色素材!いろつけようのあかいはた@色付け用の赤いバター}
\index{beurre@beurre!colorant rouge@--- Colorant rouge}
\index{colorant@colorant!beurre rouge@Beurre --- rouge}
\index{rouge@rouge!beurre colorant@Beurre colorant ---}





出来るだけ沢山の甲殻類の殻などの残りをまとめて用意する。殻の内側、外側
に張り付いている膜などをきれいに取り除く。よく乾燥させてから、鉢[^15]
に入れて細かく粉砕して、同じ重さのバターを加える。これを\ruby{湯煎}{バンマリ}にかけてよ
く混ぜながら溶かす。氷水を入れた陶製の器に、布で\ruby{漉}{こ}し入れる。固まったバ
ターをトーション[^16]で包み、余計な水を絞り出す。

###### 【原注】 {#nota-beurre-colorant-rouge}

この色付け用のバターを作るのに用いる甲殻類の殻がどうしてもない場合は、
[パプリカバター](#beurre-de-paprika)を用いてもいいだろう。だがいずれに
せよ、どんなソースであっても、仕上がりの色合いを決めるには、出来るだけ、
他の植物由来の赤色着色料の使用は避けることを勧める[^17]。

[^15]: 伝統的には大理石製の鉢が用いられることが多かった。

[^16]: [ソース・ヴェルト](#sauce-verte)訳注参照。

[^17]: この原注は第三版から。原文le rouge colorant végétal直訳すると
    「植物由来の赤色着色料」だが。ここではおそらくカルミン色素（コチニー
    ル色素）のことと思われる（[本節「概説」参
    照](#observation-sur-les-beurres-composes)）。他に赤系着色料として、
    ベニバナ色素、紅麹などもあるが、いずれも中国や日本において発達しこ
    とを考慮すると、両大戦間である1920年頃に「避けるべき」というほど普
    及していたのは、実際には昆虫由来であるコチニール色素と思われる。な
    お、ベニバナ色素も化学的にはカルミン酸色素。また、甲殻類の殻を茹で
    ると赤くなるが、この色素はアスタキサンチンといい、1938年に物質とし
    て「発見」された。もちろんエスコフィエをはじめとする料理人は経験上、
    甲殻類の殻を適度に加熱することで、たんぱく質と結びついていたアスタ
    キサンチンがたんぱく質の熱変性によって遊離して取り出せることを経験
    的によく知っており、それを利用してこの赤いバターを考案したと考えら
    れる。ちなみにサーモン、鮭の身の赤色もおなじアスタキサンチンによる
    もので、近縁種の鱒と同様に本来は白身。









\atoaki{}

#### 色付け用の緑のバター {#beurre-colorant-vert}

<div class="frsubenv">Beurre Colorant vert</div>

\index{みとり@緑 ⇒ ヴェール/ヴェルト!いろつけようのはた@色付け用の--のバター}
\index{あわせはた@合わせバター!いろつけようのみとりのはた@色付け用の緑のバター}
\index{ちやくしよくそざい@着色素材!いろつけようのみとりのはた@色付け用の緑のバター}
\index{beurre@beurre!colorant vert@--- Colorant vert}
\index{colorant@colorant!beurre vert@Beurre --- vert}
\index{vert@vert(e)!beurre colorantl@Beurre colorant ---}





ほうれんそうの葉1 kgをよく洗い、しっかり振って水気をきる。これを鉢に入
れてすり潰す。トーション[^18]で包んで緑の汁を絞り出す。これをソテー鍋
に入れて\ruby{湯煎}{バンマリ}にかけ、水分を蒸発させてペースト状にする[^19]。

これを、ぴんと張ったナフキンの上に移し、さらに水気をきる。

パレットナイフを使って緑の色素を集め、鉢に入れてその倍の重さのバターを加えて練り込む。

……布で\ruby{漉}{こ}し、冷蔵保存する。

###### 【原注】 {#nota-beurre-colorant-vert}

人工的な色素よりもこの緑の色素を用いたほうが利点が大きい。


[^18]: [ソース・ヴェルト](#sauce-verte)訳注参照。

[^19]: 原文 coaguler 凝固させる、の意。ここでは説明的に意訳した。
    なお、ほうれんそうに限らず、植物の緑色は葉緑素（クロロフィル）によ
    るものであり、葉緑素はマグネシウム（苦土）を核として窒素が周囲に結
    びついた構造を持つ化学物質。ほうれんそうの緑が濃いのは土壌からのマ
    グネシウム吸収能力が高いため。食品に含まれるマグネシウムはカルシウ
    ムの吸収を促す作用があるとされている。ただし、フランスの伝統的なほ
    うれんそうの栽培方法は、夏の終わりから初秋にかけた種を蒔き、11月頃
    から大きくなった葉を順次かき取って収穫するというもの。露地栽培でも
    1株で3回程度は春になるまでに収穫できるとされた。なお、フランス語で
    食材および調理したほうれんそうは常に épinard**s** と複数形で表され
    る。古くは14世紀の『ル・メナジエ・ド・パリ』にespinarsという綴りで
    言及があり「ポレ（ブレット）の一種で、葉が長くて茎が細く、緑は普通
    のポレよりも濃い。espinochesとも言う。四旬節の初め頃に食べる」
    (t.2, p.141)とある。中世・ルネサンス期のフランスでは、ほうれんそう
    の普及はなかなか進まなかったようで、16世紀末にオリヴィエ・ド・セー
    ルが『農業経営論』において「比較的新しい野菜」として栽培方法も含め
    て紹介している。また、épinard の語源をオリヴィエ・ド・セールは種子
    が尖っている（épineux エピヌー）からだと書いているが、実際には、こ
    の野菜が西アジア起源のものであり、ペルシャ語<!--の aspanāḫ -->から
    フランス語に入って現在の épinard という語形に至ったと考えられてい
    る。いっぽう、日本のほうれんそうはごく一部の地域を除いては、戦後高
    度成長期に普及した葉菜のひとつであり、じつのところ歴史は浅い。しば
    しば言われる東洋系、西洋系の違いにしても、普及当初からその交配品種
    が使われるようになっていたために、あまり意味はない。日本で青果とし
    て流通しているほうれんそうのほとんどは、密植、立性にして比較的若獲
    り（農協などの出荷団体によって違うが、概（おおむ）ね草丈25cm程度、
    5 株から 10 株で 200 g の規格が平均的）のため、用いている品種がほ
    ぼ西洋系のものを交配親としている場合でも、立性に栽培するために、葉
    の厚みなどはあまり問題とされていない。上述のように、フランスではか
    つて、葉以外を可食部として見なさず、軸を切り捨てるのが普通だったこ
    とと比べると、食文化の違いがよくわかる一例だろう。





\atoaki{}

#### クルヴェットバター {#beurre-de-crevette}

<div class="frsubenv">Beurre de crevette</div>


\index{あわせはた@合わせバター!くるうえつとはた@クルヴェットバター}
\index{くるうえつと@クルヴェット!はた@---バター}
\index{beurre@beurre!crevette@--- de Crevette}
\index{crevette@crevette!beurre@Beurre de ---}




[クルヴェット・グリーズ\*](#crevettes-gls) 150 gを鉢に入れて細かくすり
潰す。バター 150 gを加えて、布で\ruby{漉}{こ}す。








\atoaki{}

#### エシャロットバター {#beurre-d-echalote}

<div class="frsubenv">Beurre d'Echalote</div>


\index{あわせはた@合わせバター!えしやろつとはた@エシャロットバター}
\index{えしやろつと@エシャロット!はた@---バター}
\index{beurre@beurre!echalote@--- d'Echalote}
\index{echalote@echalote!beurre@Beurre d' ---}


エシャロット125 gを手早く[ブロンシール\*](#blanchir-gls)して湯をきり、
トーションに包んで圧すようにして水気を取り除く。これを鉢に入れてすり潰
す。バター125gを加えて、布で\ruby{漉}{こ}す。







\atoaki{}

#### エクルヴィスバター {#beurre-d-ecrevisse}

<div class="frsubenv">Beurre d'Ecrevisse</div>


\index{あわせはた@合わせバター!えくるういすはた@エクルヴィスバター}
\index{えくるういす@エクルヴィス!はた@---バター}
\index{beurre@beurre!ecrevisse@--- d'Ecrevisse}
\index{ecrevisse@écrevisse!beurre@Beurre d'---}

[ビスク](#bisque-ou-coulis-d-ecrevisses)を作る要領で、[ミルポ
ワ](#mirepoix)とともに火をとおした[エクルヴィス\*](#ecrevisses-gls)の
胴や\ruby{殻}{から}、尾などを鉢に入れて細かくすり潰す。これと同じ重さ
のバターを加え、布で\ruby{漉}{こ}す。










\atoaki{}

#### エスカルゴ用バター {#beurre-pour-les-escargots}

<div class="frsubenv">Beurre pour les Escargots</div>


\index{あわせはた@合わせバター!えすかるこようはた@エスカルゴ用バター}
\index{えすかるこ@エスカルゴ!はた@---用バター}
\index{beurre@beurre!escargots@--- pour les escargots}
\index{escargot@escargot!beurre@Beurre pour les ---s}

（エスカルゴ50個分）

バター350 gに、細かい[アシェ\*](#hacher-gls)にしたエシャロット35 gと、にんにく1片を
すり潰したペースト、パセリのアシェ25 g（大さじ1杯）、塩
12 g、こしょう2 gを加える。捏ねるようにしてよく混ぜ合わせ、冷蔵する。







\atoaki{}

#### エストラゴンバター {#beurre-d-estragon}

<div class="frsubenv">Beurre d'Estragon</div>


\index{あわせはた@合わせバター!えすとらこんはた@エストラゴンバター}
\index{えすとらこん@エストラゴン!はた@---バター}
\index{beurre@beurre!estragon@--- d'Estragon}
\index{estragon@estragon!beurre@Beurre d'---}

新鮮なエスゴラゴンの葉125 gを2分間[ブロンシール\*](#blanchir-gls)して
から湯きりして冷水にさらす。圧して余分な水気を絞る。これを鉢に入れてす
り潰す。バター125 gを加えて、布で\ruby{漉}{こ}す。






\atoaki{}

#### にしんバター {#beurre-de-hereng}

<div class="frsubenv">Beurre de Hareng</div>


\index{あわせはた@合わせバター!にしんはた@にしんバター}
\index{にしん@にしん!はた@---バター}
\index{beurre@beurre!hareng@--- de Hareng}
\index{hareng@hareng!beurre@Beurre de ---}

にしんの\ruby{燻製}{くんせい}のフィレ[^22]3枚の皮を\ruby{剥}{む}
いて、[デ\*](#des-gls)に刻む。鉢に入れて細かくすり潰す。バター250 gを
加え、布で\ruby{漉}{こ}す。


[^22]: 原文 hareng saur（アロンソール）。タイセイヨウニシンの内臓を抜
    いて10日程塩漬けにし、塩抜き後に24〜48時間乾燥させてから15時間以上、
    32℃程度で冷燻にしたもの。強い匂いが特徴。日本のにしんとは種が異な
    ること、スモークサーモンと同様に冷燻であることに注意。







\atoaki{}

#### オマールバター {#beurre-de-homard}

<div class="frsubenv">Beurre de Homard</div>


\index{あわせはた@合わせバター!おまるはた@オマールバター}
\index{おまる@オマール!はた@---バター}
\index{beurre@beurre!homard@--- de Homard}
\index{homard@homard!beurre@Beurre de ---}


使える範囲の量のオマールの胴のクリーム状の部分と卵やコライユ[^23]を鉢
に入れてすり潰す。それと同じ重さのバターを加え、布で\ruby{漉}{こ}す。

[^23]: オマールの胴の背側にある朱色の内子。





\atoaki{}

#### 白子バター {#beurre-de-laitance}

<div class="frsubenv">Beurre de Laitance</div>


\index{あわせはた@合わせバター!しらこはた@白子バター}
\index{しらこ@白子!はた@---バター}
\index{beurre@beurre!laitance@--- de Laitance}
\index{laitance@laitance!beurre@Beurre de ---}

[ポシェ\*](#pocher-gls)してからよく冷ました白子[^24]125 gを鉢に入れてすり潰す。
バター250 gとマスタード小さじ1杯を加えて、布で\ruby{漉}{こ}す。

[^24]: 日本ではスケトウダラの白子が一般的だが、フランスの伝統的高級料理では鯉
    の白子がもっとも一般的。他に鯖やにしんの白子も用いられる。







\atoaki{}

#### メートルドテルバター[^25] {#beurre-a-la-maitre-d-hotel}

<div class="frsubenv">Beurre à la Maître-d'hôtel</div>


\index{あわせはた@合わせバター!めとるとてるはた@メートルドテルバター}
\index{めとるとてる@メートルドテル!はた@---バター}
\index{beurre@beurre!maitre hotel@--- à la Maître d'hôtel}
\index{maitre hotel@maître d'hôtel!beurre@Beurre à la ---}


バター250 gをポマード状に柔らかくする。パセリの[アシェ\*](#hacher-gls)大さじ1杯
強と塩8 g、こしょう1 g、レモン $\frac{1}{4}$個分の果汁を加えてよく混ぜ合わ
せる。

###### 【原注】 {#nota-beurre-maitre-d-hotel}

このメートルドテルバターに大さじ1杯のマスタードを加えるのもバリエーショ
ンとしてお\ruby{勧}{すす}め。とりわけ牛、羊肉や魚のグリル焼きによく合う。


[^25]: メートルドテル maître d'hôtel とは直訳すれば「館\[やかた\]の主」
    あるいは「館の指導者」の意だが、時代および王家あるいは貴族やブルジョ
    ワの館、近現代のレストランにおいてそれぞれ異なった意味で用いられる
    職名。（1）王家においては grand maîtreグランメートルを補佐する仕事
    として食卓関連の仕事を取り仕切る職のこと。王と親しくすることが出来
    るために、有力貴族がこの職に就くことを希望することが多かったという。
    （2）大貴族や大ブルジョワの館において、食材の手配やワインの管理、
    料理人の選抜などの一切を取り仕切り、とりわけ宴席においてはメニュー
    作りが重要な仕事のひとつとして課される職。歴史上もっとも有名なメー
    トルドテルのひとり、ヴァテルはこの職に相当する。コンデ公に仕え、シャ
    ンティイ城でルイ14世らを招いて数日にわたって開催された千人規模大宴
    席の一切を取り仕切り、最後に手配した魚が届かないと誤解して自害した。
    彼が息を引きとってからすぐ後に魚は大量に届けられた、という（[ソー
    ス・シャンティイ](#sauce-chantilly)訳注も参照）。（3）近代から20世
    紀中葉にかけて、とりわけ料理人がオーナーではないレストランの場合は
    メニューの決定、ソムリエおよび給仕人の指揮、客の応対などを担当し、
    その店で最高のサービス技術を誇る者のつく職名とされた。なお、現代で
    はほとんど「給仕長」程度の意味しか持たなくなってしまった職名といえ
    る。上記を総合すると、この beurre à la maître d'hôtel という名称は
    「当家（当店）特製のバター」あるいは「当家（当店）自慢のバター」程
    度の意味ということになる。実際のところ、この名称の由来などは不明だ
    が、たとえばフランソワ・マランFrançois Marin（生没年不詳）の著書、
    『コモス神の贈り物』のタイトルページに記された著者の肩書は「スビー
    ズ元帥のメートルドテル、フランソワ・マラン」となっているように、本
    来はもっとも料理に精通した者の就く役職であった。このため、maître
    d'hôtel-cuisinier という語も18、19世紀には用いられていた。つまり、
    直接的に包丁を握り鍋を振ることはなくても、献立を組み、料理のレシピ
    を考えるのもまたメートルドテルの重要な仕事であった。それを踏まえて
    カレームは1822年に、それ以前の主要な宴席の献立を詳細に分析した『フ
    ランスのメートルドテル』を出版した。つまり、カレームもまた、食卓外
    交の裏側でメートルドテル=キュイジニエの役割を果たしていたというこ
    とになる。カレームをたんなるパティシエや料理人という現代的な狭い職
    の枠にはめて捉えることの出来ない時代だったとも言えよう。それは、エ
    スコフィエについても言えることであり、初版および第二版の末尾には献
    立例が掲載され、第三版以降は『メニューの本』として独立させたが、総
    料理長であるということは即ちかつて貴族の館に仕えたメートルドテルの
    仕事を勤めるに他ならない、ということを示唆しているし、その点は現代
    の一流ホテルにおいてもあまり変化していないと思われる。
    この合わせバターの名称も含めた原型のひとつとして、上述のマラン『コ
    モス神の贈り物』第2巻には、「いんげん豆のメートルドテル風」という
    レシピがある。これは水から茹でたいんげん豆を湯をきってから鍋に入れ、
    バター、パセリ、エシャロットの細かい[アシェ\*](#hacher-gls)、塩、こしょうで味付
    けし、最後にレモン果汁かヴィネガー少々で仕上げるというもの(p.380)。
    カレームの未完の名著『19世紀フランス料理』第3巻では、「鯖用のメー
    トルドテルバター」として、イジニー産バター8オンス（約250 g弱）と大
    きめのレモン1個分の搾り汁、細かくアシェしたパセリ大さじ2杯、塩2つまみ
    強、細かく挽いたこしょう1つまみ弱を木杓子を使ってよく混ぜ合わせる。
    食欲がわくような調味を心掛けるべし、とある(pp.128-129)。また、同じ
    くヴィアールの『王国料理の本』（内容は1806年初版の『帝国料理の本』
    の改訂版であり、毎年のように改版され続けているために歴史的に貴重な
    史料）1846年版では、冷製メートルドテルとして、鍋に $\frac{1}{4}$ポンド
    のバターとパセリ少々、エシャロットのアシェ少々、塩、粗挽きこしょ
    う、レモン果汁を入れ、木杓子でよく練る。これを肉料理あるいは魚料理
    の下にでも、中にでも、上にでも流すといい、とある(p.48)。このように、
    19世紀前半にはメートルドテルバターの性格がほぼ定着していたと言えよ
    う。






\atoaki{}

#### ブールマニエ {#beurre-manie}

<div class="frsubenv">Beurre Manié</div>


\index{あわせはた@合わせバター!ふるまにえ@ブールマニエ}
\index{ふるまにえ@ブールマニエ}
\index{beurre@beurre!manie@--- Manié}
\index{beurre manie@beurre manié}

これはマトロットの煮汁などに、手早くとろみ付けをするのに用いる。小麦粉75 gにバター100 gの割合が原則[^31]。

ブールマニエでとろみを付けたソースは、その後は出来るだけ沸騰させないこ
と。さもないと、生の小麦粉の不快な味が強まる危険性があるからだ。

[^31]: このバターと小麦粉の割合は絶対というわけではなく、本書でもしば
    しば異なる割合で作ったブールマニエを用いる指示が見られる。





\atoaki{}

#### ブール・マルシャンドヴァン[^30] {#beurre-marchand-de-vin}

<div class="frsubenv">Beurre Marchand de vin</div>


\index{あわせはた@合わせバター!ふるまるしやんとうあん@ブール・マルシャンドヴァン}
\index{わいんしようにん@ワイン商人 ⇒ マルシャンドヴァン!ふる@ブール・マルシャンドヴァン}
\index{まるしやんとうあん@マルシャンドヴァン!ふる@ブール・マルシャンドヴァン}
\index{beurre@beurre!marchand vin@--- Marchand de vin}
\index{marchand vin@marcand de vin!beurre@Beurre ---}

赤ワイン2 dLに細かい[アシェ\*](#hacher-gls)にしたエシャロット25 gを加えて半量になる
まで煮詰める。塩1つまみ、挽きたて[^29]（または粗く砕いた[^28]）こしょ
う1つまみ、溶かした[グラスドヴィアンド](#glace-de-viande)大さじ1杯、ポ
マード状に柔らかくしたバター150 g、レモン$\frac{1}{4}$個分の果汁とパセリ
のアシェ大さじ1杯を加える。全体をよく混ぜ合わせる。

……グリル焼きした牛リブロース[^27]用。

[^27]: 原文 entrecôte grillé（オントルコットグリエ）。

[^28]: 原文 mignonette （ミニョネット）。ミルを用いずに、包丁の側面な
    どで圧し砕いたこしょうを指す。

[^29]: 原文 poivre de moulin （ポワーヴルドゥムラン）、直訳すると「ミル
    で挽いたこしょう」だが、その場合は即座に使用するのが一般的なので、
    あえて「挽きたてのこしょう」と訳している。

[^30]: 「ワイン商人風」の意。煮詰めた赤ワインをバターに混ぜ込むところからの名称だろう。









\atoaki{}

#### ムニエル用バター {#beurre-a-la-meuniere}

<div class="frsubenv">Beurre à la Meunière</div>


\index{あわせはた@合わせバター!むにえるようはた@ムニエル用バター}
\index{むにえる@ムニエル!はた@---用バター}
\index{beurre@beurre!meuniere@--- à la Meunière}
\index{meuniere@meunière (à la)!beurre@Beurre à la ---}


焦がしバターに、提供直前にレモン果汁数滴を加えたもの。

……魚の「ムニエル[^32]」用。

[^32]: 小麦粉をまぶして、バターで焼く手法および仕立て。原文にある à la
    meunière を直訳すると「粉挽き女風」の意。かつては主に水車を動力と
    して石臼などを用い小麦を挽き、その後「ふるい」にかけていた。粉挽き
    職人は小麦粉の粉塵をかぶって真っ白になっていることが多かったところ
    から付いた料理名。







\atoaki{}

#### モンペリエバター[^33] {#beurre-de-montpellier}

<div class="frsubenv">Beurre de Montpellier</div>


\index{あわせはた@合わせバター!もんへりえはた@モンペリエバター}
\index{もんへりえ@モンペリエ!はた@---バター}
\index{beurre@beurre!monpellier@--- de Montpellier}
\index{montepellier@Montpellier!beurre@Beurre de ---}


銅の鍋に湯を沸かし、クレソンの葉とパセリの葉、セルフイユ、シブレット、
エスゴラゴンを同量ずつ計90〜100 gと、ほうれんそうの葉25 gを投入する。
これとは別の鍋で同時に、エシャロットの細かい[アシェ\*](#hacher-gls)40 gを[ブロンシール\*](#blanchir-gls)する。
ハーブは湯をきって冷水にさらす。しっかり圧し絞って余計な水気を取り除く。
エシャロットも同様にする。これらを鉢に入れてすり潰す。

中くらいのサイズのコルニション3個と、水気を絞ったケイパー大さじ1杯、小
さなにんにく1片、アンチョビのフィレ4枚を加える。全体が滑らかなペースト
状になったら、バター750 gとで卵の黄身3個、生の卵黄2個を加える。混
ぜながら、最後に植物油2 dLを少しずつ加える。目の細かい\ruby{漉}{こ}し器か布で漉し、
泡立て器で混ぜて滑らかにする。塩味を\ruby{調}{ととの}え、カイエンヌご
く少量で風味を引き締める。

……魚の冷製料理に添える。ビュッフェの場合には魚に\ruby{覆}{おお}いかけて供する。

[^33]: 南フランスの都市。モンプリエのようにも発音される。どちらも正しい。






\atoaki{}

#### 装飾用モンペリエバター {#beurre-de-montpellier-pour-croutonnage-de-plats}

<div class="frsubenv">Beurre de Montpellier pour Croûtonnage de plats</div>


\index{あわせはた@合わせバター!そうしよくようもんへりえはた@装飾用モンペリエバター}
\index{もんへりえ@モンペリエ!そうしよくようはた@装飾用---バター}
\index{beurre@beurre!beurre monpellier cretonnage@--- de Montpellier pour croûtonnage de plats}
\index{montepellier@Montpellier!beurre cretonnage@Beurre de --- pour Croûtonnage de plats}

モンペリエバターを装飾のためだけに作る場合には、植物油と茹で卵の黄身、
生の卵黄は用いないでつくる。平皿に流し入れて均等な厚みにしてやると細部の装
飾作業が容易になる。






\atoaki{}

#### マスタードバター {#beurre-de-moutarde}

<div class="frsubenv">Beurre de Moutarde</div>


\index{あわせはた@合わせバター!ますたとはた@マスタードバター}
\index{ますたと@マスタード!はた@---バター}
\index{beurre@beurre!moutarde@--- de Moutarde}
\index{moutarde@moutarde!beurre@Beurre de ---}

フランス産マスタード大さじ1 $\frac{1}{2}$杯をポマード状に柔らかくした
バター250 gに混ぜ込み、冷蔵する。







\atoaki{}

#### 格式ある宴席用ブールノワール[^33bis] {#beurre-noir-pour-les-grands-services}

<div class="frsubenv">Beurre noir pour les grands services</div>


\index{あわせはた@合わせバター!かくしきあるえんせきようふるのわる@格式ある宴席用ブールノワール}
\index{のわる@ノワール!はた@---バター}
\index{はた@バター!ふるのわる@ノワール}
\index{beurre@beurre!noir grands services@--- noir pour les grands services}
\index{noir@noir(e)!beurre@Beurre --- pour les grands services}

（仕上がり10人分[^34]）

バター125 gをフライパンに入れて火にかけて溶かし、茶色くなるまで加熱す
る。布で漉して\ruby{湯煎}{バンマリ}にかける。\ruby{微温}{ぬる}くなったら、粗く砕いたこ
しょう[^35]を加えて煮詰めたヴィネガー小さじ1杯を加える。提供直前に、丁
度いい温度になるまで温めなおす。揚げたパセリの葉とケイパー大さじ1杯を
料理にのせてから、この焦がしバターをかけてやる。

[^33bis]: 直訳すると、「大規模で格式の高い宴席において供する黒バター」。
    かつてのフランス式サービスによる宴席ではルルヴェと呼ばれる非常に壮
    麗な装飾を施した肉料理、魚料理がポタージュの後に供された。このバター
    はそういったケースを想定している。実際、カレームが焦がしバターにつ
    いてこの「黒バター」beurre noir という表現を好んで用いていたことか
    らも、『料理の手引き』の時代においてはやや大時代的な、過去の華やか
    な宴席のためのもの、というイメージだったと考えられる。


[^34]: 原文 proportion pour un service （プロポルスィオンプーランセル
    ヴィス）、直訳すると「1サーヴィスの分量」。17、18世紀から20世紀初
    頭にかけて宴席での人数の単位に service （セルヴィス）という語があ
    てられた。原則として8〜12人分とされたが、ごく大雑把に10人前と捉え
    ていい。舞踏会も含め、大規模で華やかな宴席が頻繁に行なわれていた時
    代においては、ある程度大まかに料理の単位を決めておくことで、食材の
    手配から仕込み、調理などを効率化していた。このため『料理の手引き』
    のレシピのほとんどは仕上がり量が1 serviceになるよう記されている。


[^35]: mignonette （ミニョネット）。こしょうの粒を肉叩きや包丁の側面な
    どで押し潰して砕いたもの。






\atoaki{}

#### ブールドゥノワゼット[^35bis] {#beurre-de-noisette}

<div class="frsubenv">Beurre de noisette</div>


\index{あわせはた@合わせバター!ふるとうのわせつと@ブールドゥノワゼット}
\index{のわせつと@ノワゼット!ふるとうのわせつと@ブールドゥ---}
\index{beurre@beurre!noisette@--- de noisette}
\index{noisette@noisette!beurre@Beurre de ---}

⇒ [ブール・ダヴリーヌ](#beurre-d-aveline)参照。

[^35bis]: 焦がしバターのことを一般にbeurre noisette（ブールノワゼッ
ト）と呼ぶので、混同しないよう注意。なお、ノワゼット noisette とはセイ
ヨウハシバミのこと。





\atoaki{}

#### パプリカバター {#beurre-de-paprika}

<div class="frsubenv">Beurre de Paprika</div>


\index{あわせはた@合わせバター!はふりかはた@パプリカバター}
\index{はふりか@パプリカ!はた@---バター}
\index{beurre@beurre!Paprika@--- de Paprika}
\index{paprika@paprika!beurre@Beurre de ---}

玉ねぎの[アシェ\*](#hacher-gls)大さじ1杯とパプリカ4 gをバターでいい色合いになるまで
炒め、ポマード状に柔らかくしておいたバター250 gに混ぜる。布で\ruby{漉}{こ}す。






\atoaki{}

#### 赤ピーマンバター {#beurre-de-pimentos}

<div class="frsubenv">Beurre de Pimentos</div>


\index{あわせはた@合わせバター!あかひまんはた@赤ピーマンパプリカバター}
\index{あかひまん@赤ピーマン!はた@---バター}
\index{ほわうろん@ポワヴロン ⇒ 赤ピーマン}
\index{beurre@beurre!pimentos@--- de Pimentos}
\index{pimentos@pimentos!beurre@Beurre de ---}
\index{poivron@poivron ⇒ pimento!beurre pimentos!Beurre de Pimentos}


ブレゼ[^36]した赤いポワヴロン[^37]100 gをバター250 gと合わせ
て細かくすり潰し、布で\ruby{漉}{こ}す。

[^36]: 野菜のブレゼの方法については[第13章野菜料
理](#braisage-des-legumes)参照。

[^37]: 原文 poivron。日本の青果では「パプリカ」と呼ばれる肉厚で苦みの
    少ない品種。「カリフォルニア・ワンダー」が代表的品種。未熟なものは
    緑色だが完熟すると真っ赤になる。また、熟すと黄色、紫などになる品種
    もある。








\atoaki{}

#### ピスタチオバター {#beurre-de-pistache}

<div class="frsubenv">Beurre de Pistache</div>


\index{あわせはた@合わせバター!ひすたちおはた@ピスタチオバター}
\index{ひすたちお@ピスタチオ!はた@---バター}
\index{beurre@beurre!pistache@--- de Pistache}
\index{pistache@pistache!beurre@Beurre de ---}


\ruby{殻}{から}から出して細かくすり潰す。パター250 gを加え、布で\ruby{漉}{こ}す。







\atoaki{}

#### ポーランド風バター {#beurre-a-la-polonaise}

<div class="frsubenv">Beurre à la Polonaise</div>


\index{あわせはた@合わせバター!ほらんとふうはた@ポーランド風バター}
\index{ほらんと@ポーランド!はた@---風バター}
\index{beurre@beurre!polonaise@--- à la Polonaise}
\index{polonais@polonais(e)!beurre@Beurre à la ---e}

バター250 gを\kenten{はしばみ色}[^39]になるまで火を通す。丁度いい色合いに
なったら、上等なパンの身60 gを投入する。


[^39]: 原文 cuire à la noisette （キュイーラノワゼット）すなわち「茶色
    く」なるまで火を通すということ。現代では、焦がしバターのことを
    beurre noisette （ブールノワゼット）と呼ぶことが多いが、本書におい
    ては[ブールドノワゼット](#beurre-de-noisette)という項目を立てて
    いるために、混同を避ける意味で、このような表現になっていると思われ
    る。





\atoaki{}

#### レフォールバター[^38] {#beurre-de-raifort}

<div class="frsubenv">Beurre de Raifort</div>



\index{あわせはた@合わせバター!れふおる@レフォールバター}
\index{れふおる@レフォール!はた@---バター}
\index{beurre@beurre!raifort@--- de Raifort}
\index{raifort@raifort!beurre@Beurre de ---}

[ラペ\*](#raper-gls)したレフォール50 gを鉢に入れてすり潰す。バター250 gを
加え、布で\ruby{漉}{こ}す。


[^38]: ホースラディッシュ、西洋わさび。







\atoaki{}

#### ブール・ラヴィゴット/ブール・ヴェール {#beurre-ravigote}

<div class="frsubenv">Beurre Ravigote ou Beurre vert</div>


\index{あわせはた@合わせバター!らういこつと@ブール・ラヴィゴット}
\index{あわせはた@合わせバター!うえる@ブール・ヴェール}
\index{みとり@緑 ⇒ ヴェール/ヴェルト!ブール・ヴェール}
\index{らういこつと@ラヴィゴット!ふる@ブール・---}
\index{beurre@beurre!ravigote@--- Ravigote}
\index{beurre@beurre!vert@--- vert}
\index{ravigote@ravigote!beurre@Beurre ---}
\index{vert@vert(e)!beurre@Beurre ---}

⇒ [ブール・シヴリ](#beurre-chivry)参照。





\atoaki{}

#### スモークサーモンバター {#beurre-de-saumon-fume}

<div class="frsubenv">Beurre de Saumon fumé</div>


\index{あわせはた@合わせバター!すもくさもん@スモークサーモンバター}
\index{すもくさもん@スモークサーモン!はた@---バター}
\index{beurre@beurre!saumon fumet@--- de Saumon fumé}
\index{saumon fume@saumon fumé!beurre@Beurre de ---}


スモークサーモン100 gとバター250 gを鉢に入れてよくすり潰し、布で\ruby{漉}{こ}す。






\atoaki{}

#### トリュフバター {#beurre-de-truffe}

<div class="frsubenv">Beurre de Truffe</div>


\index{あわせはた@合わせバター!とりゆふ@トリュフバター}
\index{とりゆふ@トリュフ!はた@---バター}
\index{beurre@beurre!truffe@--- de Truffe}
\index{truffe@truffe!beurre@Beurre de ---}



真黒な黒トリュフ100 gと[ベシャメルソース](#sauce-bechamel)小さじ1杯を
鉢に入れてすり潰す。良質なバター200 gを加え、布で\ruby{漉}{こ}す。








\atoaki{}

#### ブール・プランタニエ[^43] {#beurres-printaniers}

<div class="frsubenv">Beurres Printaniers</div>


\index{あわせはた@合わせバター!ふるとふらんたにえ@ブール・プランタニエ}
\index{ふらんたにえ@プランタニエ!ふる@ブール・---}
\index{beurre@beurre!printaniers@--- Printaniers}
\index{printanier@printanier(ère)!beurre@Beurres Printaniers}

野菜の合わせバターはポタージュやソースの仕上げによく用いられる。

野菜はまず、それぞれの種類に応じた方法で火を通すこと。例えば、にんじん
やナヴェ[^40]の場合はバターを加えて弱火で[エチュヴェ\*](#etuver-gls)してから[コンソ
メ](#consomme-blanc-simple)で火をとおす。緑の野菜、例えばプチポワ[^42]、
さやいんげん[^41]、アスパラガスの穂先などの場合は、しっかり[ブロ
ンシール](#blanchir-gls)して火をとおすこと。

その後、野菜と同じ重さのバターとともに鉢に入れてすり潰し、布で\ruby{漉}{こ}す。

[^40]: 原文 navet 蕪のことだが、日本の蕪とは調理特性および風味が異なるので注意。

[^41]: 原文 haricots verts （アリコヴェール）。

[^42]: 原文 petits pois いわゆるグリンピースのことだが、20世紀以降、だ
    んだん若どりのものが好まれる傾向が強まっており、日本で一般的なグリ
    ンピースと比較するとかなり小さめの段階で収穫されるものが多く、火入
    れに必要な時間もごく短かい傾向にある。直径7〜8mm程度の若獲りのもの
    はグリンピース特有の青臭さが少なく、フレッシュであれば生食でも美味
    しい。フランスあるいはイタリア産の冷凍品が多く出回っているが加熱必
    須。


[^43]: printanier （プランタニエ）春の、を意味する語で、とりわけ
    春先の「はしり」の野菜を用いる場合によくこの表現があてられる。








\atoaki{}

#### クリ[^44] {#coulis-divers}

<div class="frsubenv">Coulis divers</div>


\index{くうり@クーリ |see {くり@クリ}}
\index{くり@クリ}
\index{coulis divers@Coulis divers}


* [エクルヴィス\*](#ecrevisses-gls)の殻、または

* [クルヴェット\*](#crevettes-gls)の胴や殻、または

* オマールや[ラングスト\*](#langouste-gls)の胴にあるクリーム状の部分や卵、[コライユ\*](#corail-gls)

を鉢に入れてすり潰す。

すり潰した材料 100 gあたり大さじ4杯の新鮮な生クリームを加え、布で\ruby{漉}{こ}す。

これらのクリは提供直前に用いること。使い方はこの「合わせバター」の節冒
頭に記しておいたので参照されたい。


[^44]: coulis （クリ）の基本概念としては、ピュレよりは水分の多いもの、
    と解していいのだが、ここではやや特殊な用法となっていることに注意。
    18世紀には「ソース」とほぼ同義としてよくこの語が用いられた。また、
    日本の調理現場では「クーリ」と呼ぶ傾向が根強く残っている。しかし、
    フランス語として見たとき、この語それ自体のアクセント（フランス語の
    アクセントは長音）は最後の i の音にある。ou （発音記号/ u /）は日
    本語にない音で、多くの日本人の耳には強く感じられるために、このよう
    な習慣が付いたのだと思われる。少なくとも日本語的な発音で「クーリ」
    と覚えても、フランス語としては通じない可能性が高いので注意。




\atoaki{}

#### 甲殻類のオイル {#huile-de-crustaces}

<div class="frsubenv">Huile de Crustacés</div>

\index{あふら@油 ⇒ オイル!こうかくるい@甲殻類の---}
\index{おいる@オイル!こうかくるい@甲殻類の---}
\index{こうかくるい@甲殻類!オイル@甲殻類のオイル}
\index{huile@huile!crustaces@--- de Crustacés}
\index{crustace@crustacé!huile@Huile de ---s}


このレシピは、オマールやラングストに添えるマヨネーズの仕上げに加えるた
め考案されたもので、言ってみればマヨネーズの新しいバリエーションだ。

使えるだけの甲殻類の殻などをすり潰し、バターではなくオイルを同量、つま
り甲殻類と同じ重さだけ加える。つまり、オイルの重さが 1 dLあたり 95 gくら
い、あるいは、すり潰した甲殻類の殻が大さじ 6 杯に対してオイル 1 dLという
ことになる。

甲殻類の殻をすり潰してペースト状になってきたら、\ruby{すりこ木}{ピロン}でよく混ぜなが
ら油を少量ずつ加えていく。

まず目の細かい\ruby{漉}{こ}し器で漉す。さらに布で漉してから冷やしておく。このオイルは
絶対に熱を与えてはいけない。

\index{あわせはた@合わせバター|)}
\index{beurre@beurre!beurres composes@---s Composés|)}

</div><!--endRecette-->

<!--20190405江畑校正確認スミ-->
